# README #

AWSの指定したEC2サーバーを停止するスクリプトです。


### How do I get set up? ###

* IAMユーザーの作成
	* CLI用IAMユーザーを作成
	* IAMユーザーに権限付与
* AWS CLI操作用アクセスキーの取得
* AWS EC2で各サーバにタグを付ける
* AWS CLIの設定
	* AWS CLIのインストール
	* AWS CLIの初期設定
	* AWS CLIの実行確認
* バッチの作成(認証情報、サーバー情報の書き換え)

